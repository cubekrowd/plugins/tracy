Tracy client for Paper
======================

This is an experimental client for Tracy for Paper written for fun. I'm not sure if it's robust enough for production use. You'll need to install and start the Tracy server application yourself; see https://github.com/wolfpld/tracy.

Before attempting to build this project with `mvn clean package`, make sure you have Paper 1.16.5 (not the API!) installed in your local Maven repository or modify the repository in the 'pom.xml'. The plugin may not work on your machine due to weird stuff in the Tracy client C library I haven't figured out. It works fine for me on macOS though. 
