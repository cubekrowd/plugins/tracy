set -x

if [ ! -e "lib/tracy" ]; then
    # download Tracy if not present
    TRACY_VER="0.7.6"

    mkdir -p lib
    wget -O lib/tracy.zip https://github.com/wolfpld/tracy/archive/v${TRACY_VER}.zip
    unzip -q lib/tracy.zip -d lib
    mv lib/tracy-${TRACY_VER} lib/tracy
    rm lib/tracy.zip
fi

# Seem to need fPIC on ARM for dynamic library
CFLAGS="-O3 -flto -fPIC -shared -DTRACY_ENABLE -DTRACY_ON_DEMAND -DTRACY_ONLY_LOCALHOST"
INCLUDES="-I lib -I${JAVA_HOME}/include"

# jni.h includes jni_md.h, which is included in an OS-specific folder
case "$OSTYPE" in
    darwin*) INCLUDES+=" -I${JAVA_HOME}/include/darwin";;
    linux*) INCLUDES+=" -I${JAVA_HOME}/include/linux";;
esac

cc $CFLAGS -c src/main/c/*.c $INCLUDES
c++ $CFLAGS -std=c++11 -c lib/tracy/TracyClient.cpp

LIBS="-ljvm -lpthread -ldl"

# no lib atomic on macOS
case "$OSTYPE" in
    darwin*) LIBS+="";;
    *) LIBS+=" -latomic";;
esac

c++ $CFLAGS -L"${JAVA_HOME}/lib/server" -o ${MVN_OUTPUT_DIR}/libtracy.so *.o $LIBS
