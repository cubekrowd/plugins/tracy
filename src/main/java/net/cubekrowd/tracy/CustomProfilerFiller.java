package net.cubekrowd.tracy;

import java.util.function.Supplier;
import java.util.logging.Level;
import net.minecraft.server.v1_16_R3.GameProfilerFillerActive;
import net.minecraft.server.v1_16_R3.GameProfilerSwitcher;
import net.minecraft.server.v1_16_R3.MethodProfilerResults;
import net.minecraft.server.v1_16_R3.MethodProfilerResultsEmpty;
import net.minecraft.server.v1_16_R3.MinecraftServer;

public class CustomProfilerFiller implements GameProfilerFillerActive {
    public ThreadLocal<Boolean> isThreadKnown = new ThreadLocal<>();

    public void insertIntoMinecraft() {
        try {
            var server = MinecraftServer.getServer();
            var continuousProfilerField = MinecraftServer.class.getDeclaredField("m");
            continuousProfilerField.setAccessible(true);
            var continuousProfiler = (GameProfilerSwitcher) continuousProfilerField.get(server);
            var profilerFilledField = continuousProfiler.getClass().getDeclaredField("c");
            profilerFilledField.setAccessible(true);
            profilerFilledField.set(continuousProfiler, new CustomProfilerFiller());
        } catch (IllegalAccessException | NoSuchFieldException e) {
            TracyPlugin.INSTANCE.getLogger().log(Level.SEVERE, "Failed to edit profiler", e);
        }
    }

    public void removeFromMinecraft() {
        // stop profiling: resets the profiler filler referenced in the
        // continuous profiler to the noop one
        MinecraftServer.getServer().aU();
    }

    // startTick
    @Override
    public void a() {
        Tracy.markFrame();
    }

    // endTick
    @Override
    public void b() {
        // nothing
    }

    public void initThread() {
        if (isThreadKnown.get() == null) {
            isThreadKnown.set(true);
            Tracy.initThread();
        }
    }

    public void beginZone(String zoneName, int traceIndex) {
        initThread();

        boolean enable = true;
        if (zoneName.equals("nextTickWait")) {
            // don't show this in Tracy
            enable = false;
        }

        // @NOTE(traks) Getting the Java stack trace is ultra slow!!!
        // Turns < 1ms tick unto 8ms

        // var caller = Thread.currentThread().getStackTrace()[traceIndex];
        // Tracy.beginZone(zoneName, caller.getFileName(), caller.getMethodName(), caller.getLineNumber(), enable);
        Tracy.beginZone(zoneName, "", zoneName, 0, enable);
    }

    // push
    @Override
    public void enter(String s) {
        beginZone(s, 3);
    }

    // push
    @Override
    public void a(Supplier<String> supplier) {
        beginZone(supplier.get(), 3);
    }

    // pop
    @Override
    public void exit() {
        initThread();

        Tracy.endZone();
    }

    // popPush
    @Override
    public void exitEnter(String s) {
        exit();
        beginZone(s, 3);
    }

    // incrementCounter
    @Override
    public void c(String s) {
        // for counting how often something happens. we don't use this
    }

    // incrementCounter
    @Override
    public void c(Supplier<String> supplier) {
        c(supplier.get());
    }

    @Override
    public MethodProfilerResults d() {
        return MethodProfilerResultsEmpty.a;
    }
}
