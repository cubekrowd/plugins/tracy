package net.cubekrowd.tracy;

import com.destroystokyo.paper.event.server.ServerTickEndEvent;
import com.destroystokyo.paper.event.server.ServerTickStartEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class TracyPlugin extends JavaPlugin implements Listener {
    public static TracyPlugin INSTANCE;
    public CustomProfilerFiller profiler = new CustomProfilerFiller();
    public boolean didTick;

    @Override
    public void onEnable() {
        INSTANCE = this;
        Bukkit.getPluginManager().registerEvents(this, this);
        profiler.insertIntoMinecraft();
    }

    @Override
    public void onDisable() {
        profiler.removeFromMinecraft();
        INSTANCE = null;
    }

    @EventHandler
    public void onTickStart(ServerTickStartEvent e) {
        Tracy.markFrame();
        Tracy.beginZone("tick", "", "tick", 0, true);
        didTick = true;
    }

    @EventHandler
    public void onTickEnd(ServerTickEndEvent e) {
        if (didTick) {
            Tracy.endZone();
        }
        didTick = false;
    }
}
