package net.cubekrowd.tracy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import org.bukkit.Bukkit;

public class Tracy {
    static {
        File tempFile = null;
        try {
            tempFile = File.createTempFile("tracyplugin-", null);
            try (var is = Tracy.class.getResourceAsStream("/libtracy.so")) {
                Files.copy(is, tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
            System.load(tempFile.getAbsolutePath());
        } catch (IOException e) {
            Bukkit.getPluginManager().getPlugin("Tracy").getLogger().log(Level.SEVERE, "Failed to load libtracy.so", e);
        } finally {
            if (tempFile != null) {
                tempFile.delete();
            }
        }
    }

    public static native void markFrame();

    public static native void beginZone(String zoneName, String sourceFile, String functionName, int lineNumber, boolean enable);

    public static native void endZone();

    public static native void initThread();
}
