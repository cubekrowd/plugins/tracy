#include <tracy/TracyC.h>
#include <jni.h>

static TracyCZoneCtx tracy_contexts[256];
static int tracy_context_count;

jint
JNI_OnLoad(JavaVM * vm, void * reserved) {
    jint jni_version = JNI_VERSION_10;
    return jni_version;
}

JNIEXPORT void JNICALL
Java_net_cubekrowd_tracy_Tracy_initThread(JNIEnv * env, jobject thisObj) {
    ___tracy_init_thread();
}

JNIEXPORT void JNICALL
Java_net_cubekrowd_tracy_Tracy_markFrame(JNIEnv * env, jobject thisObj) {
    ___tracy_emit_frame_mark(0);
}

JNIEXPORT void JNICALL
Java_net_cubekrowd_tracy_Tracy_beginZone(JNIEnv * env, jobject thisObj, jstring jname, jstring jsource, jstring jfunction, jint jline, jboolean enable) {
    const char * name = (*env)->GetStringUTFChars(env, jname, NULL);
    int name_len = (*env)->GetStringUTFLength(env, jname);

    const char * source = (*env)->GetStringUTFChars(env, jsource, NULL);
    int source_len = (*env)->GetStringUTFLength(env, jsource);

    const char * function = (*env)->GetStringUTFChars(env, jfunction, NULL);
    int function_len = (*env)->GetStringUTFLength(env, jfunction);

    int i = tracy_context_count;
    tracy_context_count++;

    uint64_t srcloc = ___tracy_alloc_srcloc(jline, source, source_len, function, function_len);

    TracyCZoneCtx * ctx = tracy_contexts + i;
    *ctx = ___tracy_emit_zone_begin_alloc(srcloc, enable ? 1 : 0);
    ___tracy_emit_zone_name(*ctx, name, name_len);

    (*env)->ReleaseStringUTFChars(env, jname, name);
    (*env)->ReleaseStringUTFChars(env, jsource, source);
    (*env)->ReleaseStringUTFChars(env, jfunction, function);
}

JNIEXPORT void JNICALL
Java_net_cubekrowd_tracy_Tracy_endZone(JNIEnv * env, jobject thisObj) {
    tracy_context_count--;
    int i = tracy_context_count;

    ___tracy_emit_zone_end(tracy_contexts[i]);
}
